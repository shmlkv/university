#include <stdio.h>
#include <math.h>

int main() {
    float a, b;
    printf("Введите первое число для обработки формулой:\n");
    scanf ("%f", &a);

    printf("Введите второе число для обработки формулой:\n");
    scanf ("%f", &b);

    float result = sqrt(fabs((a * a) + (b * b)) / 5);
    printf("Ответ: %f", result);
    return 0;
}