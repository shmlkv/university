#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

int main() {
    float r;

    printf("Введите радиус для вычисления:\n");

    scanf ("%f", &r);
    if(r < 0){
        printf("Простите, но радиус не может быть отрицательным");
    }else{
        printf("Площадь круга: %f\n", PI * pow(r, 2));
        printf("Длина окружности: %f\n", 2 * PI * r);
        printf("Объем шара: %f\n", 4.0 / 3.0 * PI * pow(r, 3));
    }
    

    return 0;
}