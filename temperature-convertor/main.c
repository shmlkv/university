

#include <stdio.h>
#include <stdlib.h>

void getC (float t);
void getF (float t);
void getK (float t);

int main(int argc, char *argv[]) {
    float t = atof(argv[1]);
    char * sc = argv[2];
    if(argv[2]) {
        if (*sc == 'C') {
            getC(t);
        }else if (*sc == 'F') {
            getF(t);
        }else if (* sc == 'K'){
            getK(t);
        }
    }else{
        printf("%.0f C:\n", t);
        getC(t);

        printf("%.0f F:\n", t);
        getF(t);

        printf("%.0f K:\n", t);
        getK(t);

    }

    return 0;
}
void getC (float t){
    if (t < -273.15){
        printf("Error. Temp < 273.15 C\n");
    }else{
        printf("%.2f  K\n", t +273.15);
        printf("%.2f F\n\n", t * 9/5 + 32.0);
    }
}
void getF (float t){
    if (t < -459){
        printf("Error. Temp < -459 F F\n");
    }else{
        printf("%.2f K\n", (t  - 32.0) * 5/9 + 273.15);
        printf("%.2f C\n\n", (t  - 32.0) * 5/9);
    }
}
void getK (float t){
    if (t < 0){
        printf("Error. Temp < 0 K\n");

    }else{
        printf("%.2f C\n", t -273.15);
        printf("%.2f  F\n", (t  - 273.15) * 9/5 + 32.0);
    }

}
